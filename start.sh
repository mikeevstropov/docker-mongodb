#!/usr/bin/env bash

# stop container
docker-compose down --rmi all

# update source
# git pull

# start container
docker-compose up -d
